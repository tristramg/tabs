pdf : ps
	ps2pdf songs.ps

iconv :
	iconv -futf8 -tlatin9 songs.cho > songs.latin9.cho

ps : iconv
	chordii -a -i -P a4 songs.latin9.cho > songs.ps

clean :
	rm songs.latin9.cho songs.ps songs.pdf
